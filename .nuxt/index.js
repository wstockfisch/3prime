import 'es6-promise/auto'
import Vue from 'vue'
import Meta from 'vue-meta'
import { createRouter } from './router.js'
import NoSSR from './components/no-ssr.js'
import NuxtChild from './components/nuxt-child.js'
import NuxtLink from './components/nuxt-link.js'
import NuxtError from './components/nuxt-error.vue'
import Nuxt from './components/nuxt.js'
import App from './App.js'
import { setContext, getLocation, getRouteData } from './utils'


/* Plugins */
import nuxt_plugin_i18nseoplugin_5a179272 from 'nuxt_plugin_i18nseoplugin_5a179272' // Source: ./i18n.seo.plugin.js
import nuxt_plugin_i18nroutingplugin_6eefffca from 'nuxt_plugin_i18nroutingplugin_6eefffca' // Source: ./i18n.routing.plugin.js
import nuxt_plugin_i18nroutesutils_753dcae9 from 'nuxt_plugin_i18nroutesutils_753dcae9' // Source: ./i18n.routes.utils.js
import nuxt_plugin_i18nplugin_36224b7a from 'nuxt_plugin_i18nplugin_36224b7a' // Source: ./i18n.plugin.js
import nuxt_plugin_axios_35d8e130 from 'nuxt_plugin_axios_35d8e130' // Source: ./axios.js
import nuxt_plugin_vuetify_e5914fcc from 'nuxt_plugin_vuetify_e5914fcc' // Source: ../plugins/vuetify
import nuxt_plugin_i18n_66ff12a5 from 'nuxt_plugin_i18n_66ff12a5' // Source: ../plugins/i18n.js
import nuxt_plugin_capitalize_32779274 from 'nuxt_plugin_capitalize_32779274' // Source: ../plugins/filters/capitalize
import nuxt_plugin_ga_fb0a2534 from 'nuxt_plugin_ga_fb0a2534' // Source: ../plugins/ga.js (ssr: false)
import nuxt_plugin_gtm_63eb8124 from 'nuxt_plugin_gtm_63eb8124' // Source: ../plugins/gtm.js (ssr: false)


// Component: <no-ssr>
Vue.component(NoSSR.name, NoSSR)

// Component: <nuxt-child>
Vue.component(NuxtChild.name, NuxtChild)

// Component: <nuxt-link>
Vue.component(NuxtLink.name, NuxtLink)

// Component: <nuxt>`
Vue.component(Nuxt.name, Nuxt)

// vue-meta configuration
Vue.use(Meta, {
  keyName: 'head', // the component option name that vue-meta looks for meta info on.
  attribute: 'data-n-head', // the attribute name vue-meta adds to the tags it observes
  ssrAttribute: 'data-n-head-ssr', // the attribute name that lets vue-meta know that meta info has already been server-rendered
  tagIDKeyName: 'hid' // the property name that vue-meta uses to determine whether to overwrite or append a tag
})

const defaultTransition = {"name":"fade","mode":"out-in","appear":false,"appearClass":"appear","appearActiveClass":"appear-active","appearToClass":"appear-to"}

async function createApp (ssrContext) {
  const router = createRouter(ssrContext)

  

  // Create Root instance
  // here we inject the router and store to all child components,
  // making them available everywhere as `this.$router` and `this.$store`.
  const app = {
    router,
    
    nuxt: {
      defaultTransition,
      transitions: [ defaultTransition ],
      setTransitions (transitions) {
        if (!Array.isArray(transitions)) {
          transitions = [ transitions ]
        }
        transitions = transitions.map((transition) => {
          if (!transition) {
            transition = defaultTransition
          } else if (typeof transition === 'string') {
            transition = Object.assign({}, defaultTransition, { name: transition })
          } else {
            transition = Object.assign({}, defaultTransition, transition)
          }
          return transition
        })
        this.$options.nuxt.transitions = transitions
        return transitions
      },
      err: null,
      dateErr: null,
      error (err) {
        err = err || null
        app.context._errored = !!err
        if (typeof err === 'string') err = { statusCode: 500, message: err }
        const nuxt = this.nuxt || this.$options.nuxt
        nuxt.dateErr = Date.now()
        nuxt.err = err
        // Used in lib/server.js
        if (ssrContext) ssrContext.nuxt.error = err
        return err
      }
    },
    ...App
  }
  
  const next = ssrContext ? ssrContext.next : location => app.router.push(location)
  // Resolve route
  let route
  if (ssrContext) {
    route = router.resolve(ssrContext.url).route
  } else {
    const path = getLocation(router.options.base)
    route = router.resolve(path).route
  }

  // Set context to app.context
  await setContext(app, {
    route,
    next,
    error: app.nuxt.error.bind(app),
    
    payload: ssrContext ? ssrContext.payload : undefined,
    req: ssrContext ? ssrContext.req : undefined,
    res: ssrContext ? ssrContext.res : undefined,
    beforeRenderFns: ssrContext ? ssrContext.beforeRenderFns : undefined
  })

  const inject = function (key, value) {
    if (!key) throw new Error('inject(key, value) has no key provided')
    if (!value) throw new Error('inject(key, value) has no value provided')
    key = '$' + key
    // Add into app
    app[key] = value
    
    // Check if plugin not already installed
    const installKey = '__nuxt_' + key + '_installed__'
    if (Vue[installKey]) return
    Vue[installKey] = true
    // Call Vue.use() to install the plugin into vm
    Vue.use(() => {
      if (!Vue.prototype.hasOwnProperty(key)) {
        Object.defineProperty(Vue.prototype, key, {
          get () {
            return this.$root.$options[key]
          }
        })
      }
    })
  }

  

  // Plugin execution
  
  if (typeof nuxt_plugin_i18nseoplugin_5a179272 === 'function') await nuxt_plugin_i18nseoplugin_5a179272(app.context, inject)
  if (typeof nuxt_plugin_i18nroutingplugin_6eefffca === 'function') await nuxt_plugin_i18nroutingplugin_6eefffca(app.context, inject)
  if (typeof nuxt_plugin_i18nroutesutils_753dcae9 === 'function') await nuxt_plugin_i18nroutesutils_753dcae9(app.context, inject)
  if (typeof nuxt_plugin_i18nplugin_36224b7a === 'function') await nuxt_plugin_i18nplugin_36224b7a(app.context, inject)
  if (typeof nuxt_plugin_axios_35d8e130 === 'function') await nuxt_plugin_axios_35d8e130(app.context, inject)
  if (typeof nuxt_plugin_vuetify_e5914fcc === 'function') await nuxt_plugin_vuetify_e5914fcc(app.context, inject)
  if (typeof nuxt_plugin_i18n_66ff12a5 === 'function') await nuxt_plugin_i18n_66ff12a5(app.context, inject)
  if (typeof nuxt_plugin_capitalize_32779274 === 'function') await nuxt_plugin_capitalize_32779274(app.context, inject)
  
  if (process.browser) { 
    if (typeof nuxt_plugin_ga_fb0a2534 === 'function') await nuxt_plugin_ga_fb0a2534(app.context, inject)
    if (typeof nuxt_plugin_gtm_63eb8124 === 'function') await nuxt_plugin_gtm_63eb8124(app.context, inject)
  }

  // If server-side, wait for async component to be resolved first
  if (process.server && ssrContext && ssrContext.url) {
    await new Promise((resolve, reject) => {
      router.push(ssrContext.url, resolve, () => {
        // navigated to a different route in router guard
        const unregister = router.afterEach(async (to, from, next) => {
          ssrContext.url = to.fullPath
          app.context.route = await getRouteData(to)
          app.context.params = to.params || {}
          app.context.query = to.query || {}
          unregister()
          resolve()
        })
      })
    })
  }

  return {
    app,
    router,
    
  }
}

export { createApp, NuxtError }
