import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const _66d0d557 = () => import('../pages/single-feed-ingredients/index.vue' /* webpackChunkName: "" */).then(m => m.default || m)
const _614ac3b0 = () => import('../pages/samples/index.vue' /* webpackChunkName: "" */).then(m => m.default || m)
const _9e0c2bd8 = () => import('../pages/complete-feed-rations/index.vue' /* webpackChunkName: "" */).then(m => m.default || m)
const _65e3adac = () => import('../pages/about/index.vue' /* webpackChunkName: "" */).then(m => m.default || m)
const _41e9917f = () => import('../pages/contact/index.vue' /* webpackChunkName: "" */).then(m => m.default || m)
const _1cf66e7d = () => import('../pages/options/index.vue' /* webpackChunkName: "" */).then(m => m.default || m)
const _62afbdd4 = () => import('../pages/service/index.vue' /* webpackChunkName: "" */).then(m => m.default || m)
const _7f61d4ae = () => import('../pages/index.vue' /* webpackChunkName: "" */).then(m => m.default || m)



if (process.client) {
  window.history.scrollRestoration = 'manual'
}
const scrollBehavior = function (to, from, savedPosition) {
  // if the returned position is falsy or an empty object,
  // will retain current scroll position.
  let position = false

  // if no children detected
  if (to.matched.length < 2) {
    // scroll to the top of the page
    position = { x: 0, y: 0 }
  } else if (to.matched.some((r) => r.components.default.options.scrollToTop)) {
    // if one of the children has scrollToTop option set to true
    position = { x: 0, y: 0 }
  }

  // savedPosition is only available for popstate navigations (back button)
  if (savedPosition) {
    position = savedPosition
  }

  return new Promise(resolve => {
    // wait for the out transition to complete (if necessary)
    window.$nuxt.$once('triggerScroll', () => {
      // coords will be used if no selector is provided,
      // or if the selector didn't match any element.
      if (to.hash) {
        let hash = to.hash
        // CSS.escape() is not supported with IE and Edge.
        if (typeof window.CSS !== 'undefined' && typeof window.CSS.escape !== 'undefined') {
          hash = '#' + window.CSS.escape(hash.substr(1))
        }
        try {
          if (document.querySelector(hash)) {
            // scroll to anchor by returning the selector
            position = { selector: hash }
          }
        } catch (e) {
          console.warn('Failed to save scroll position. Please add CSS.escape() polyfill (https://github.com/mathiasbynens/CSS.escape).')
        }
      }
      resolve(position)
    })
  })
}


export function createRouter () {
  return new Router({
    mode: 'history',
    base: '/',
    linkActiveClass: 'nuxt-link-active',
    linkExactActiveClass: 'nuxt-link-exact-active',
    scrollBehavior,
    routes: [
		{
			path: "/single-feed-ingredients",
			component: _66d0d557,
			name: "single-feed-ingredients-en"
		},
		{
			path: "/fr/single-feed-ingredients",
			component: _66d0d557,
			name: "single-feed-ingredients-fr"
		},
		{
			path: "/es/single-feed-ingredients",
			component: _66d0d557,
			name: "single-feed-ingredients-es"
		},
		{
			path: "/pt/single-feed-ingredients",
			component: _66d0d557,
			name: "single-feed-ingredients-pt"
		},
		{
			path: "/ja/single-feed-ingredients",
			component: _66d0d557,
			name: "single-feed-ingredients-ja"
		},
		{
			path: "/sw/single-feed-ingredients",
			component: _66d0d557,
			name: "single-feed-ingredients-sw"
		},
		{
			path: "/ar/single-feed-ingredients",
			component: _66d0d557,
			name: "single-feed-ingredients-ar"
		},
		{
			path: "/bn/single-feed-ingredients",
			component: _66d0d557,
			name: "single-feed-ingredients-bn"
		},
		{
			path: "/fa/single-feed-ingredients",
			component: _66d0d557,
			name: "single-feed-ingredients-fa"
		},
		{
			path: "/samples",
			component: _614ac3b0,
			name: "samples-en"
		},
		{
			path: "/fr/samples",
			component: _614ac3b0,
			name: "samples-fr"
		},
		{
			path: "/es/samples",
			component: _614ac3b0,
			name: "samples-es"
		},
		{
			path: "/pt/samples",
			component: _614ac3b0,
			name: "samples-pt"
		},
		{
			path: "/ja/samples",
			component: _614ac3b0,
			name: "samples-ja"
		},
		{
			path: "/sw/samples",
			component: _614ac3b0,
			name: "samples-sw"
		},
		{
			path: "/ar/samples",
			component: _614ac3b0,
			name: "samples-ar"
		},
		{
			path: "/bn/samples",
			component: _614ac3b0,
			name: "samples-bn"
		},
		{
			path: "/fa/samples",
			component: _614ac3b0,
			name: "samples-fa"
		},
		{
			path: "/complete-feed-rations",
			component: _9e0c2bd8,
			name: "complete-feed-rations-en"
		},
		{
			path: "/fr/complete-feed-rations",
			component: _9e0c2bd8,
			name: "complete-feed-rations-fr"
		},
		{
			path: "/es/complete-feed-rations",
			component: _9e0c2bd8,
			name: "complete-feed-rations-es"
		},
		{
			path: "/pt/complete-feed-rations",
			component: _9e0c2bd8,
			name: "complete-feed-rations-pt"
		},
		{
			path: "/ja/complete-feed-rations",
			component: _9e0c2bd8,
			name: "complete-feed-rations-ja"
		},
		{
			path: "/sw/complete-feed-rations",
			component: _9e0c2bd8,
			name: "complete-feed-rations-sw"
		},
		{
			path: "/ar/complete-feed-rations",
			component: _9e0c2bd8,
			name: "complete-feed-rations-ar"
		},
		{
			path: "/bn/complete-feed-rations",
			component: _9e0c2bd8,
			name: "complete-feed-rations-bn"
		},
		{
			path: "/fa/complete-feed-rations",
			component: _9e0c2bd8,
			name: "complete-feed-rations-fa"
		},
		{
			path: "/about",
			component: _65e3adac,
			name: "about-en"
		},
		{
			path: "/fr/about",
			component: _65e3adac,
			name: "about-fr"
		},
		{
			path: "/es/about",
			component: _65e3adac,
			name: "about-es"
		},
		{
			path: "/pt/about",
			component: _65e3adac,
			name: "about-pt"
		},
		{
			path: "/ja/about",
			component: _65e3adac,
			name: "about-ja"
		},
		{
			path: "/sw/about",
			component: _65e3adac,
			name: "about-sw"
		},
		{
			path: "/ar/about",
			component: _65e3adac,
			name: "about-ar"
		},
		{
			path: "/bn/about",
			component: _65e3adac,
			name: "about-bn"
		},
		{
			path: "/fa/about",
			component: _65e3adac,
			name: "about-fa"
		},
		{
			path: "/contact",
			component: _41e9917f,
			name: "contact-en"
		},
		{
			path: "/fr/contact",
			component: _41e9917f,
			name: "contact-fr"
		},
		{
			path: "/es/contact",
			component: _41e9917f,
			name: "contact-es"
		},
		{
			path: "/pt/contact",
			component: _41e9917f,
			name: "contact-pt"
		},
		{
			path: "/ja/contact",
			component: _41e9917f,
			name: "contact-ja"
		},
		{
			path: "/sw/contact",
			component: _41e9917f,
			name: "contact-sw"
		},
		{
			path: "/ar/contact",
			component: _41e9917f,
			name: "contact-ar"
		},
		{
			path: "/bn/contact",
			component: _41e9917f,
			name: "contact-bn"
		},
		{
			path: "/fa/contact",
			component: _41e9917f,
			name: "contact-fa"
		},
		{
			path: "/options",
			component: _1cf66e7d,
			name: "options-en"
		},
		{
			path: "/fr/options",
			component: _1cf66e7d,
			name: "options-fr"
		},
		{
			path: "/es/options",
			component: _1cf66e7d,
			name: "options-es"
		},
		{
			path: "/pt/options",
			component: _1cf66e7d,
			name: "options-pt"
		},
		{
			path: "/ja/options",
			component: _1cf66e7d,
			name: "options-ja"
		},
		{
			path: "/sw/options",
			component: _1cf66e7d,
			name: "options-sw"
		},
		{
			path: "/ar/options",
			component: _1cf66e7d,
			name: "options-ar"
		},
		{
			path: "/bn/options",
			component: _1cf66e7d,
			name: "options-bn"
		},
		{
			path: "/fa/options",
			component: _1cf66e7d,
			name: "options-fa"
		},
		{
			path: "/service",
			component: _62afbdd4,
			name: "service-en"
		},
		{
			path: "/fr/service",
			component: _62afbdd4,
			name: "service-fr"
		},
		{
			path: "/es/service",
			component: _62afbdd4,
			name: "service-es"
		},
		{
			path: "/pt/service",
			component: _62afbdd4,
			name: "service-pt"
		},
		{
			path: "/ja/service",
			component: _62afbdd4,
			name: "service-ja"
		},
		{
			path: "/sw/service",
			component: _62afbdd4,
			name: "service-sw"
		},
		{
			path: "/ar/service",
			component: _62afbdd4,
			name: "service-ar"
		},
		{
			path: "/bn/service",
			component: _62afbdd4,
			name: "service-bn"
		},
		{
			path: "/fa/service",
			component: _62afbdd4,
			name: "service-fa"
		},
		{
			path: "/",
			component: _7f61d4ae,
			name: "index-en"
		},
		{
			path: "/fr",
			component: _7f61d4ae,
			name: "index-fr"
		},
		{
			path: "/es",
			component: _7f61d4ae,
			name: "index-es"
		},
		{
			path: "/pt",
			component: _7f61d4ae,
			name: "index-pt"
		},
		{
			path: "/ja",
			component: _7f61d4ae,
			name: "index-ja"
		},
		{
			path: "/sw",
			component: _7f61d4ae,
			name: "index-sw"
		},
		{
			path: "/ar",
			component: _7f61d4ae,
			name: "index-ar"
		},
		{
			path: "/bn",
			component: _7f61d4ae,
			name: "index-bn"
		},
		{
			path: "/fa",
			component: _7f61d4ae,
			name: "index-fa"
		}
    ],
    
    
    fallback: false
  })
}
