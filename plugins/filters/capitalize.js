import Vue from "vue";

const capitalize = function(value) {
  return value.replace(/\b\w/g, l => l.toUpperCase());
};

Vue.filter("capitalize", capitalize);
