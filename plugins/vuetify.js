import Vue from "vue";
import Vuetify from "vuetify";
import colors from "vuetify/es5/util/colors";

Vue.use(Vuetify, {
  theme: {
    primary: "#105B63", // Dark Blue
    accent: "#FFFAD5", // Yellow
    // secondary: "#FFD34E", // Orange
    secondary: "#274A52",
    info: "#DB9E36", // Gold
    warning: "#EB7F00",
    error: "#BD4932", // Brick
    success: "#FF5252"
  }
});
