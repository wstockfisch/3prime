const pkg = require("./package");

const nodeExternals = require("webpack-node-externals");

module.exports = {
  mode: "universal",
  head: {
    // htmlAttrs: {
    // lang: this.$nuxt.$i18n.locale
    // },
    title: "3Prime | Premium Quality, Purely NON-GMO and Provenance EU",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: pkg.description },
      {
        name: "description",
        content:
          "Animal Feed, Overseas Logistics, and Nutritional Advice. European produced ​and premium quality ​feed ingredients from select EU Central European sources.​"
      },
      {
        name: "keywords",
        content:
          "animal feed, nutritional advice, sugar beet pulp, non-gmo, premium quality animal feed, ruminants, cow, horse, sheep, camel"
      },
      { name: "author", content: "Wessel Stockfisch" }
    ],
    link: [
      { rel: "icon", type: "image/x-icon", href: "/favicon.ico" },
      {
        rel: "stylesheet",
        href:
          "https://fonts.googleapis.com/css?family=Barlow+Condensed:300,400,500,700|Material+Icons"
      },
      {
        rel: "stylesheet",
        href: "https://fonts.googleapis.com/css?family=Oswald"
      }
    ]
  },
  loading: { color: "#CCA059" },
  css: [
    "~assets/style/vuetify.styl",
    "~assets/style/variables.styl",
    "~assets/style/transitions.styl"
  ],
  plugins: [
    "@/plugins/vuetify",
    "@/plugins/i18n.js",
    "@/plugins/filters/capitalize",
    { src: "~plugins/ga.js", ssr: false },
    { src: "~plugins/gtm.js", ssr: false }
  ],
  modules: [
    "@nuxtjs/axios",
    [
      "nuxt-i18n",
      {
        locales: [
          { code: "en" },
          { code: "fr" },
          { code: "es" },
          { code: "pt" },
          { code: "ja" },
          { code: "sw" },
          { code: "ar" },
          { code: "bn" },
          { code: "fa" }
        ],
        defaultLocale: "en",
        vueI18n: {
          fallbackLocale: "en",
          messages: {
            en: require("./locales/en.json"),
            fr: require("./locales/fr.json"),
            es: require("./locales/es.json"),
            pt: require("./locales/pt.json"),
            ja: require("./locales/ja.json"),
            sw: require("./locales/sw.json"),
            ar: require("./locales/ar.json"),
            bn: require("./locales/bn.json"),
            fa: require("./locales/fa.json")
          }
        }
      }
    ],
    "@nuxtjs/sitemap"
  ],
  axios: {},
  build: {
    extend(config, ctx) {
      if (ctx.isServer) {
        config.externals = [
          nodeExternals({
            whitelist: [/^vuetify/]
          })
        ];
      }
    }
  },
  transition: {
    name: "fade",
    mode: "out-in"
  },
  sitemap: {
    path: "/sitemap.xml",
    hostname: "https://www.3prime.eu",
    cacheTime: 1000 * 60 * 15,
    generate: true, // Enable me when using nuxt generate
    routes: [
      {
        url: "/",
        priority: 1,
        lastmodISO: "2018-04-02T17:37:26+00:00"
      },
      {
        url: "/about",
        priority: 1,
        lastmodISO: "2018-04-02T17:37:26+00:00"
      },
      {
        url: "/samples",
        priority: 0.5,
        lastmodISO: "2018-04-02T17:37:26+00:00"
      },
      {
        url: "/single-feed-ingredients",
        priority: 0.9,
        lastmodISO: "2018-04-02T17:37:26+00:00"
      },
      {
        url: "/complete-feed-rations",
        priority: 0.9,
        lastmodISO: "2018-04-02T17:37:26+00:00"
      },
      {
        url: "/service",
        priority: 0.7,
        lastmodISO: "2018-04-02T17:37:26+00:00"
      },
      {
        url: "/options",
        priority: 0.5,
        lastmodISO: "2018-04-02T17:37:26+00:00"
      },
      {
        url: "/contact",
        priority: 0.9,
        lastmodISO: "2018-04-02T17:37:26+00:00"
      }
    ]
  }
};
